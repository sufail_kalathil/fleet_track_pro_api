# Fleet Tracker Pro 

## Table of Contents
1. [Introduction](#introduction)
2. [Architecture Overview](#architecture-overview)
3. [Technical Details](#technical-details)
4. [Modules](#modules)
    - [Vehicle Module](#vehicle-module)
    - [Maintenance Module](#maintenance-module)
    - [Tracking Module](#tracking-module)
5. [Detailed API Documentation](#detailed-api-documentation)

## Introduction
Fleet Tracker Pro is a comprehensive vehicle management system designed to streamline operations and enhance efficiency in fleet management. Leveraging modern technologies such as NestJS for backend development and React for frontend design, Fleet Tracker Pro offers robust features including vehicle tracking, maintenance logging, analytics, and real-time data synchronization with clients.
## Architecture Overview
Fleet Tracker Pro is built around a modular architecture, with a primary focus on the Vehicle Module. This module encompasses various components such as Tracking, Analytics, and Maintenance, each tightly integrated with the core functionality of vehicle management.- **Vehicle Module**: Manages basic operations related to vehicle management and operation.
- **Vehicle Module**: Handles basic operations related to vehicle management and operation.
- **Maintenance Component**: Facilitates maintenance scheduling and tracking of maintenance details..
- **Tracking Module**: Tracks vehicle data in real-time, synchronizing information like speed and current location with clients.
- **Analytics Component**: Provides basic analytics functionalities for better insights into fleet operations..

Real-time data synchronization with clients is achieved using WebSocket.


### Backend
- **Framework**: NestJS
- **Database**: MongoDB
- **ORM**: Mongoose
- **Design Patterns**: Dependency Injection, Repository Pattern
- **Websockets**: Used for real-time data synchronization

### Frontend
- **Framework**: React
- **UI Kit**: React-Bootstrap
- **State Management**: Redux, Redux Saga for efficient state management
- **Websockets**: Used to connect with the backend for real-time data sync

## Technical Details
In the backend, NestJS and MongoDB are used. Mongoose is utilized for interacting with the database, and decorators like `@Schema` and `@Table` are used for schema creation. Dependency Injection is employed for managing dependencies, and the Repository Pattern is implemented for data access.

In the frontend, React is used along with React-Bootstrap for UI components. Redux and Redux Saga are employed for efficient state management. Socket.io is used to connect with the backend for real-time data.

## Modules

### Vehicle Module
The Vehicle Module facilitates vehicle management, allowing users to create, update, and delete vehicles. Each vehicle can be defined by its model, brand, year, type, and registration number, with registration numbers being unique identifiers. Validation ensures data integrity, and vehicles can be set as active, inactive, or in maintenance status.

#### Vehicle API Endpoints

| Request Type | URL                           | Description                            |
|--------------|-------------------------------|----------------------------------------|
| POST         | /vehicle                   | Create a new vehicle                   |
| GET          | /vehicle                   | Get all vehicles                       |
| GET          | /vehicle/{id}              | Get a vehicle by ID                    |
| PUT          | /vehicle/{id}              | Update a vehicle by ID                 |
| DELETE       | /vehicle/{id}              | Delete a vehicle by ID                 |
| GET          | /vehicle/{id}/analytics    | Get vehicle analytics by ID            |

### Maintenance Module
The Maintenance Module allows users to log maintenance records for each vehicle, providing insights into the cost and frequency of maintenance activities. Maintenance logs include details such as service cost, type, and date, enabling efficient tracking and analysis of vehicle maintenance expenses.

#### Maintenance API Endpoints

| Request Type | URL                          | Description                                  |
|--------------|------------------------------|----------------------------------------------|
| POST         | /maintenance             | Log maintenance records of a vehicle         |
| GET          | /maintenance             | Get all maintenance logs                     |
| GET          | /maintenance/{id}        | Get maintenance logs for a vehicle by ID     |
| PUT          | /maintenance/{id}        | Update maintenance by ID                     |
| DELETE       | /maintenance/{id}        | Delete maintenance by ID                     |
| GET          | /maintenance/analytics/vehicle/{id}/cost | Get vehicle analytics by ID       |

### Tracking Module
The Tracking Module is a crucial feature in Fleet Track Management as it includes the real-time tracking capability of vehicles. This module facilitates real-time tracking and synchronization with clients, integrating seamlessly with IoT devices through tracking APIs.

To enable tracking, the process begins by starting the tracking process, which records the current location and time of the vehicle. It's important to note that the vehicle should be in an active status to start the tracking process. An API is exposed to initiate the tracking process, accessible at tracking/vehicle/:vehicleId/start.

Once the tracking process is started, real-time data becomes available from the IoT device installed in the respective vehicle. This data includes information such as current speed and location, which are passed frequently via the log API at tracking/vehicle/:vehicleId/log.

To stop the tracking process, another API endpoint is provided at tracking/vehicle/:vehicleId/stop. Upon stopping the tracking process, the vehicle status returns to active, indicating that it is ready for the next trip and tracking session.

The tracking data collected provides valuable insights for analysis, including trip duration, total distance covered, and other analytics specific to each vehicle.
#### Tracking API Endpoints

| Request Type | URL                                    | Description                              |
|--------------|----------------------------------------|------------------------------------------|
| POST         | /tracking/vehicle/{vehicleId}/start | Start tracking a vehicle                 |
| POST         | /tracking/vehicle/{vehicleId}/log   | Log tracking data of a vehicle           |
| POST         | /tracking/vehicle/{vehicleId}/stop  | Stop tracking a vehicle                  |

## Detailed API Documentation

| Request Type | URL                                    | Description                              |
|--------------|----------------------------------------|------------------------------------------|
| POST         | /vehicle                           | Create a new vehicle                     |
| GET          | /vehicle                           | Get all vehicles                         |
| GET          | /vehicle/{id}                      | Get a vehicle by ID                      |
| PUT          | /vehicle/{id}                      | Update a vehicle by ID                   |
| DELETE       | /vehicle/{id}                      | Delete a vehicle by ID                   |
| GET          | /vehicle/{id}/analytics            | Get vehicle analytics by ID              |
| POST         | /maintenance                       | Log maintenance records of a vehicle     |
| GET          | /maintenance                       | Get all maintenance logs                 |
| GET          | /maintenance/{id}                  | Get maintenance logs for a vehicle by ID |
| PUT          | /maintenance/{id}                  | Update maintenance by ID                 |
| DELETE       | /maintenance/{id}                  | Delete maintenance by ID                 |
| GET          | /maintenance/analytics/vehicle/{id}/cost | Get vehicle analytics by ID              |
| POST         | /tracking/vehicle/{vehicleId}/start | Start tracking a vehicle                 |
| POST         | /tracking/vehicle/{vehicleId}/log   | Log tracking data of a vehicle           |
| POST         | /tracking/vehicle/{vehicleId}/stop  | Stop tracking a vehicle                  |

#### Supported Query Parameters

- **pageSize**: *(Default: 10)* Number of items per page.
- **pageNo**: *(Default: 1)* Page number.
- **paginate**: *(Default: true)* Flag to enable/disable pagination.
- **sortOrder**: *(Default: ASC)* Sorting order.
- **sortBy**: *(Default: updatedAt)* Field to sort by.