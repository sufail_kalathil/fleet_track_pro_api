import {
  AggregateOptions,
  AnyObject,
  ClientSession,
  FilterQuery,
  Model,
  PipelineStage,
  QueryOptions,
} from 'mongoose';
import { RequestDto } from './dto/Request.dto';
import { SortOrderEnum } from './enum/sort-order.enum';
import { IBaseFilter } from './abstraction/base-filter';
import { IPopulate } from './abstraction/populate';

export abstract class BaseRepository<T extends Document> {
  constructor(private readonly model: Model<T>) {}

  findAll(
    filter?: FilterQuery<T>,
    options?: QueryOptions,
    populate?: IPopulate,
    projection?: object,
  ): Promise<T[]> {
    const data = this.model.find(filter, projection, options);
    if (populate) {
      const { path, select } = populate;
      data.populate(path, select);
    }
    return data;
  }

  async findOne(
    filter?: FilterQuery<T>,
    projection?: object,
    options?: QueryOptions,
  ): Promise<T> {
    return this.model.findOne(filter, projection, options);
  }

  async findById(
    id: string,
    projection?: object,
    options?: QueryOptions,
  ): Promise<T | undefined> {
    return this.model.findById(id, projection, options);
  }

  async create(doc: T | AnyObject, session?: ClientSession): Promise<T> {
    return this.model.create(doc);
    // const record = new this.model(doc);
    // return await record.save({ session });
  }

  async update(_id: string, doc: any) {
    return this.model.findOneAndUpdate(
      { _id, isActive: true },
      { $set: doc },
      { new: true },
    );
  }

  async upsert(filter: FilterQuery<T>, doc: any) {
    return this.model.findOneAndUpdate(filter, { $set: doc }, { upsert: true });
  }

  countDocuments(filter: FilterQuery<T>) {
    return this.model.countDocuments(filter);
  }

  aggregate(pipeline: PipelineStage[], options?: AggregateOptions) {
    return this.model.aggregate(pipeline, options);
  }

  BasicQueryBuilder<T>(request: RequestDto): IBaseFilter<T> {
    const { pageNo, pageSize, paginate, sortBy, sortOrder } = request;
    const filter: FilterQuery<any> = { isActive: true };
    const options: QueryOptions = {};

    // pagination
    if (paginate) {
      options.limit = pageSize;
      options.skip = (pageNo - 1) * pageSize;
    }

    // sort
    options.sort = { [sortBy]: sortOrder === SortOrderEnum.DESC ? -1 : 1 };

    return {
      filter,
      options,
    };
  }
}
