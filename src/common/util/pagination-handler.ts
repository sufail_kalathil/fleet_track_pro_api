import { PageDto } from '../dto/page.dto';

export const paginationHandler = <T>(
  result: T[],
  count: number,
  pageNo: number,
  pageSize: number,
): PageDto<T> => {
  const totalPages = Math.ceil(count / pageSize);
  return {
    result: result,
    pagination: {
      pageNo: pageNo,
      pageSize: pageSize,
      totalPages: totalPages,
      totalItems: count,
      hasPreviousPage: pageNo > 1,
      hasNextPage: pageNo < totalPages,
    },
  };
};
