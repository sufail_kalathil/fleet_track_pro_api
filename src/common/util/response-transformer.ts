import { RequestDto } from '../dto/Request.dto';
import { PageDto } from '../dto/page.dto';
import { paginationHandler } from './pagination-handler';

export const responseTransformer = <T>(
  request: RequestDto,
  result: T[],
  count: number,
): T[] | PageDto<T> => {
  const { paginate, pageSize, pageNo } = request;

  if (!paginate) {
    return result;
  }

  return paginationHandler(result, count, pageNo, pageSize);
};
