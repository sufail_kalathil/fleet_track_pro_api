import { Expose } from 'class-transformer';
import { PageMetaDto } from './page-meta.dto';

export class PageDto<T> {
  @Expose()
  result: T[];

  @Expose()
  pagination: PageMetaDto;
}
