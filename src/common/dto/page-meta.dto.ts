import { Expose } from 'class-transformer';

export class PageMetaDto {
  @Expose()
  pageNo: number;

  @Expose()
  pageSize: number;

  @Expose()
  totalItems: number;

  @Expose()
  totalPages: number;

  @Expose()
  hasPreviousPage: boolean;

  @Expose()
  hasNextPage: boolean;
}
