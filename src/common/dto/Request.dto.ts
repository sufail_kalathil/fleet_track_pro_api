import { appConstants } from '../../constants';
import { IBaseRequest } from '../abstraction/base-request';
import {
  IsBoolean,
  IsEnum,
  IsInt,
  IsOptional,
  IsString,
  Max,
  Min,
} from 'class-validator';
import { Transform } from 'class-transformer';
import { SortOrderEnum } from '../enum/sort-order.enum';
import { SortByEnum } from '../enum/sort-by-enum';

const { PAGE_NO, PAGE_SIZE, PAGINATE, SORT_ORDER, SORT_BY } = appConstants;
export class RequestDto implements IBaseRequest {
  @IsInt()
  @Min(1)
  @Max(50)
  @Transform(({ value }) => Number(value))
  pageSize: number = PAGE_SIZE;

  @IsInt()
  @Min(1)
  @Transform(({ value }) => Number(value))
  pageNo: number = PAGE_NO;

  @IsOptional()
  @IsBoolean()
  @Transform(({ value }) => value == 'true')
  paginate: boolean = PAGINATE;

  @IsString()
  @IsEnum(SortOrderEnum)
  sortOrder: SortOrderEnum = SORT_ORDER;

  @IsString()
  @IsEnum(SortByEnum)
  sortBy: SortByEnum = SORT_BY;

  @IsOptional()
  @IsString()
  keyword?: string;

  @IsOptional()
  dateFrom?: string;

  @IsOptional()
  dateTo?: string;
}
