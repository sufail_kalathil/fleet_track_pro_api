import { ClientSession, Connection } from 'mongoose';

export const transactionManager = async <T = any>(
  method: (session: ClientSession) => Promise<T>,
  onError: (error: any) => any,
  connection: Connection,
  session?: ClientSession,
): Promise<T> => {
  const isSessionFurnished = session === undefined ? false : true;
  if (isSessionFurnished === false) {
    session = await connection.startSession();
    session.startTransaction();
  }

  let error;
  let result: T;
  try {
    result = await method(session);

    if (isSessionFurnished === false) {
      await session.commitTransaction();
    }
  } catch (err) {
    error = err;
    if (isSessionFurnished === false) {
      await session.abortTransaction();
    }
  } finally {
    if (isSessionFurnished === false) {
      await session.endSession();
    }

    if (error) {
      onError(error);
    }

    return result;
  }
};

const onError = (error: any): void => {
  throw error;
};
// return transactionManager(
//   async (session: ClientSession): Promise<any> => {
//     await this.vehicleTrackingDataRepository.create(
//       vehicleTrackingData,
//       session,
//     );
//     await this.vehicleTrackingDataRepository.create(
//       vehicleTrackingData2,
//       session,
//     );
//   },
//   onError,
//   this.connection,
// );

// const session = await this.connection.startSession();
// session.startTransaction();
//
// try {
//   const vehicleTrackingData = {
//     vehicleId,
//     speed: 20,
//     locationName: 'testName',
//     location: {
//       coordinates: [38.8951, 77.0364],
//     },
//   };
//   const vehicleTrackingData2 = {
//     vehicleId: '65c8a8e33c4465d2caa40c62',
//     speed: '',
//     locationName: 'testName',
//     location: {
//       coordinates: [38.8951, 77.0364],
//     },
//   };
//
//   await this.vehicleTrackingDataRepository.create(
//     vehicleTrackingData,
//     session,
//   );
//   // await this.vehicleTrackingDataRepository.create(
//   //   vehicleTrackingData2,
//   //   session,
//   // );
//   await session.commitTransaction();
// } catch (error) {
//   console.error('Err', error);
//   await session.abortTransaction();
// } finally {
//   await session.endSession();
// }
