import { FilterQuery, QueryOptions } from 'mongoose';

export interface IBaseFilter<T> {
  filter: FilterQuery<T>;
  options: QueryOptions;
}
