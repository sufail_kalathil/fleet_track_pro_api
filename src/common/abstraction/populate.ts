export interface IPopulate {
  path: string | string[];
  select?: string | any;
}
