import { SortOrderEnum } from '../enum/sort-order.enum';
import { SortByEnum } from '../enum/sort-by-enum';

export interface IBaseRequest {
  sortOrder: SortOrderEnum;

  sortBy: SortByEnum;

  pageNo: number;

  pageSize: number;

  paginate: boolean;

  keyword?: string;

  dateFrom?: string;

  dateTo?: string;
}
