import { MongooseModuleFactoryOptions } from '@nestjs/mongoose/dist/interfaces/mongoose-options.interface';
import { ConfigService } from '@nestjs/config';
import { envConstants } from '../constants';

export const databaseConfiguration = async (
  configService: ConfigService,
): Promise<MongooseModuleFactoryOptions> => {
  const dbName = configService.get<string>(envConstants.DB_NAME);
  const dbHost = configService.get<string>(envConstants.DB_HOST);
  const dbPort = configService.get<string>(envConstants.DB_PORT);
  const dbUri = configService.get<string>(envConstants.DB_URI);
  return {
    uri: getUri(dbUri, dbName, dbHost, dbPort),
  };
};

const getUri = (
  uri: string,
  dbName: string,
  dbHost: string,
  dbPort: string,
): string => {
  if (uri) {
    return uri;
  }
  return `mongodb://${dbHost}:${dbPort}/${dbName}`;
};
