import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { envConstants } from './constants';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // add global prefix
  app.setGlobalPrefix('/api');

  // add validation pipe
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
    }),
  );

  // enable CORS
  app.enableCors();

  const configService = app.get(ConfigService);
  const port = Number(configService.get<string>(envConstants.PORT)) || 3000;
  await app.listen(port);
}
bootstrap();
