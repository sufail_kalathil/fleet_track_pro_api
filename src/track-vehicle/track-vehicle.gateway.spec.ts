import { Test, TestingModule } from '@nestjs/testing';
import { TrackVehicleGateway } from './track-vehicle.gateway';

describe('TrackVehicleGateway', () => {
  let gateway: TrackVehicleGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TrackVehicleGateway],
    }).compile();

    gateway = module.get<TrackVehicleGateway>(TrackVehicleGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
