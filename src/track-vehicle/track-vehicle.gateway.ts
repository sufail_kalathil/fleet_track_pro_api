import {
  ConnectedSocket,
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';

@WebSocketGateway({ cors: true })
export class TrackVehicleGateway {
  @WebSocketServer() server: Socket;

  syncLogEvent(message: string): void {
    this.server.emit('trackingLog', message);
  }

  syncStartEvent(message: string) {
    this.server.emit('startEvent', message);
  }

  syncStopEvent(message: string) {
    this.server.emit('stopEvent', message);
  }
}
