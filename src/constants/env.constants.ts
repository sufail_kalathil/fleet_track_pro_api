export const envConstants = {
  PORT: 'PORT',
  DB_NAME: 'DB_NAME',
  DB_PORT: 'DB_PORT',
  DB_HOST: 'DB_HOST',
  DB_URI: 'DB_URI',
};
