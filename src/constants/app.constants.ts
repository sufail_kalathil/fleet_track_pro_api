import { SortByEnum, SortOrderEnum } from '../common';

export const appConstants = {
  PAGE_SIZE: 10,
  PAGE_NO: 1,
  PAGINATE: true,
  SORT_ORDER: SortOrderEnum.DESC,
  SORT_BY: SortByEnum.UPDATED_AT,
};
