import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateVehicleDto, UpdateVehicleDto } from './dto';
import { VehicleRepository, VehicleTrackingLogRepository } from './repository';
import { exceptionMessage } from './constants';
import { RequestDto, responseTransformer } from '../../common';

@Injectable()
export class VehicleService {
  constructor(
    private readonly vehicleRepository: VehicleRepository,
    private readonly vehicleTrackingLogRepository: VehicleTrackingLogRepository,
  ) {}
  async create(createVehicleDto: CreateVehicleDto) {
    const { uin } = createVehicleDto;
    const vehicleIsExist = await this.vehicleRepository.findOne({
      uin,
      isActive: true,
    });
    if (vehicleIsExist) {
      throw new BadRequestException(
        'vehicle already registered with the registration number',
      );
    }
    return this.vehicleRepository.create(createVehicleDto);
  }

  async findAll(request: RequestDto) {
    const { filter, options } =
      this.vehicleRepository.BasicQueryBuilder(request);
    const [count, vehicles] = await Promise.all([
      this.vehicleRepository.countDocuments(filter),
      this.vehicleRepository.findAll(filter, options),
    ]);
    return responseTransformer(request, vehicles, count);
  }

  async findById(_id: string) {
    const vehicle = await this.vehicleRepository.findOne({
      _id,
      isActive: true,
    });
    if (!vehicle) {
      throw new NotFoundException(`${exceptionMessage.VEHICLE_NOT_FOUND}`);
    }
    return vehicle;
  }

  async update(id: string, updateVehicleDto: UpdateVehicleDto): Promise<void> {
    const vehicle = await this.vehicleRepository.update(id, updateVehicleDto);
    if (!vehicle) {
      throw new NotFoundException(`${exceptionMessage.VEHICLE_NOT_FOUND}`);
    }
    return;
  }

  async updateVehicleStatus(
    id: string,
    updateVehicleDto: UpdateVehicleDto,
  ): Promise<void> {
    const vehicle = await this.vehicleRepository.update(id, updateVehicleDto);
    if (!vehicle) {
      throw new NotFoundException(`${exceptionMessage.VEHICLE_NOT_FOUND}`);
    }
    return;
  }

  async remove(id: string): Promise<void> {
    const vehicle = await this.vehicleRepository.update(id, {
      isActive: false,
    });

    if (!vehicle) {
      throw new NotFoundException(`${exceptionMessage.VEHICLE_NOT_FOUND}`);
    }

    return;
  }

  findVehicleAnalytics(vehicleId: string) {
    const pipelines = [
      {
        $match: { vehicleId },
      },
      {
        $addFields: {
          distance: {
            $sqrt: {
              $add: [
                {
                  $pow: [
                    {
                      $subtract: [
                        {
                          $arrayElemAt: ['$endLocation.coordinates', 0],
                        },
                        {
                          $arrayElemAt: ['$startLocation.coordinates', 0],
                        },
                      ],
                    },
                    2,
                  ],
                },
                {
                  $pow: [
                    {
                      $subtract: [
                        {
                          $arrayElemAt: ['$endLocation.coordinates', 1],
                        },
                        {
                          $arrayElemAt: ['$startLocation.coordinates', 1],
                        },
                      ],
                    },
                    2,
                  ],
                },
              ],
            },
          },
        },
      },
      {
        $group: {
          _id: null,
          totalDistance: {
            $sum: '$distance',
          },
          totalDuration: {
            $sum: {
              $subtract: ['$endDate', '$startDate'],
            },
          },
        },
      },
      {
        $project: {
          _id: 0,
        },
      },
    ];

    return this.vehicleTrackingLogRepository.aggregate(pipelines);
  }
}
