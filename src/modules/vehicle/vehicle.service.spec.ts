import { Test, TestingModule } from '@nestjs/testing';
import { VehicleService } from './vehicle.service';
import { VehicleRepository, VehicleTrackingLogRepository } from './repository';
import { CreateVehicleDto, UpdateVehicleDto } from './dto';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { RequestDto, SortByEnum, SortOrderEnum } from '../../common';
import { VehicleStatusEnum } from './contracts';

describe('VehicleService', () => {
  let service: VehicleService;
  let vehicleRepository: VehicleRepository;
  let vehicleTrackingLogRepository: VehicleTrackingLogRepository;

  const repositoryMockFunctions = {
    findById: jest.fn(),
    find: jest.fn(),
    findOne: jest.fn(),
    findAll: jest.fn(),
    create: jest.fn(),
    update: jest.fn(),
    countDocuments: jest.fn(),
    aggregate: jest.fn(),
    BasicQueryBuilder: jest.fn(),
  };

  const mockQueryBuilder = {
    filter: { isActive: true },
    options: {
      limit: 10,
      skip: 1,
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        VehicleService,
        {
          provide: VehicleRepository,
          useValue: repositoryMockFunctions,
        },
        {
          provide: VehicleTrackingLogRepository,
          useValue: repositoryMockFunctions,
        },
      ],
    }).compile();

    service = module.get<VehicleService>(VehicleService);
  });

  const createRequest: CreateVehicleDto = {
    brand: 'Toyota',
    model: 'Innoca',
    year: 2023,
    uin: '12121',
    color: 'Orange',
    type: 'Sedan',
    engineNumber: 'ABC123456',
  };

  const updateRequest: UpdateVehicleDto = {
    brand: 'Toyota',
    model: 'Innoca',
    year: 2023,
    uin: '12121',
    color: 'Orange',
    type: 'Sedan',
    engineNumber: 'ABC123456',
    status: VehicleStatusEnum.RUNNING,
  };

  const vehicles = [
    {
      brand: 'Toyota',
      model: 'Innoca',
      year: 2023,
      uin: '12121',
      color: 'Orange',
      type: 'Sedan',
      engineNumber: 'ABC123456',
    },
    {
      brand: 'Toyota',
      model: 'Innoca',
      year: 2023,
      uin: '12121',
      color: 'Orange',
      type: 'Sedan',
      engineNumber: 'ABC123456',
    },
  ];

  const listRequest: RequestDto = {
    paginate: false,
    pageSize: 1,
    pageNo: 10,
    sortOrder: SortOrderEnum.ASC,
    sortBy: SortByEnum.CREATED_AT,
  };

  describe('Create vehicle', () => {
    it('Should return error if the registration number already exist', async () => {
      try {
        jest.spyOn(repositoryMockFunctions, 'findOne').mockReturnValue({
          _id: 'dsd',
        });

        const result = await service.create(createRequest);
      } catch (e) {
        expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
        expect(e).toBeInstanceOf(BadRequestException);
      }
    });

    it('Should create new vehicle', async () => {
      const newVehicle = { _id: '123', brand: 'toyota' };

      jest.spyOn(repositoryMockFunctions, 'findOne').mockReturnValue(undefined);
      jest.spyOn(repositoryMockFunctions, 'create').mockReturnValue(newVehicle);

      const result = await service.create(createRequest);

      expect(result).toBe(newVehicle);
      expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
      expect(repositoryMockFunctions.create).toHaveBeenCalled();
    });
  });

  describe('List Vehicle', () => {
    it('Should list the vehicle', async () => {
      jest
        .spyOn(repositoryMockFunctions, 'BasicQueryBuilder')
        .mockReturnValue(mockQueryBuilder);

      jest.spyOn(repositoryMockFunctions, 'countDocuments').mockReturnValue(5);
      jest.spyOn(repositoryMockFunctions, 'findAll').mockReturnValue(vehicles);

      const result = await service.findAll(listRequest);

      expect(repositoryMockFunctions.BasicQueryBuilder).toBeDefined();
      expect(repositoryMockFunctions.countDocuments).toBeDefined();
      expect(repositoryMockFunctions.findAll).toBeDefined();
      expect(result).toBe(vehicles);
    });
  });

  describe('List vehicle By Id', () => {
    it('Should return error if no vehicle found', async () => {
      try {
        jest
          .spyOn(repositoryMockFunctions, 'findOne')
          .mockReturnValue(undefined);
        const result = await service.findById('dfdiiiq');
      } catch (e) {
        expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
        expect(e).toBeInstanceOf(NotFoundException);
      }
    });

    it('Should return vehicle by id', async () => {
      jest
        .spyOn(repositoryMockFunctions, 'findOne')
        .mockReturnValue(vehicles[0]);
      const result = await service.findById('dfdiiiq');

      expect(result).toBe(vehicles[0]);
      expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
    });
  });

  describe('Update Vehicle', () => {
    it('Should return error if vehicle not found', async () => {
      try {
        jest
          .spyOn(repositoryMockFunctions, 'update')
          .mockReturnValue(undefined);
        const result = await service.update('id65d', updateRequest);
      } catch (e) {
        expect(repositoryMockFunctions.update).toHaveBeenCalled();
        expect(e).toBeInstanceOf(NotFoundException);
      }
    });

    it('update vehicle', async () => {
      jest
        .spyOn(repositoryMockFunctions, 'update')
        .mockReturnValue(vehicles[0]);
      const result = await service.update('id65d', updateRequest);

      expect(result).toBe(undefined);
      expect(repositoryMockFunctions.update).toHaveBeenCalled();
    });
  });

  describe('Remove Vehicle', () => {
    it('Should return error if vehicle not found', async () => {
      try {
        jest
          .spyOn(repositoryMockFunctions, 'update')
          .mockReturnValue(undefined);
        const result = await service.remove('id65d');
      } catch (e) {
        expect(repositoryMockFunctions.update).toHaveBeenCalled();
        expect(e).toBeInstanceOf(NotFoundException);
      }
    });

    it('Remove vehicle', async () => {
      jest
        .spyOn(repositoryMockFunctions, 'update')
        .mockReturnValue(vehicles[0]);
      const result = await service.remove('id65d');

      expect(result).toBe(undefined);
      expect(repositoryMockFunctions.update).toHaveBeenCalled();
    });
  });
});
