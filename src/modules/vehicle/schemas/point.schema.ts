import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export class PointSchema {
  @Prop({ type: String, enum: ['Point'], default: 'Point' })
  type: string;

  @Prop({ type: [Number] })
  coordinates: number[];
}
