import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { VehicleStatusEnum } from '../contracts';

export type VehicleDocument = Vehicle & Document;

@Schema({
  collection: 'Vehicle',
  timestamps: true,
})
export class Vehicle {
  @Prop({ required: true })
  brand: string;

  @Prop({ required: true })
  model: string;

  @Prop({ required: true })
  year: string;

  @Prop({ required: true })
  uin: string;

  @Prop()
  color: string;

  @Prop()
  type: string;

  @Prop()
  engineNumber: string;

  @Prop({
    required: true,
    enum: [
      VehicleStatusEnum.ACTIVE,
      VehicleStatusEnum.MAINTENANCE,
      VehicleStatusEnum.RUNNING,
    ],
    default: VehicleStatusEnum.ACTIVE,
  })
  status: string;

  @Prop({
    type: Boolean,
    default: true,
  })
  isActive: boolean;
}

export const VehicleSchema = SchemaFactory.createForClass(Vehicle);
