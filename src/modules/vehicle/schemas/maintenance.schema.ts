import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Types } from 'mongoose';
import { Vehicle } from './vehicle.schema';
import * as mongoose from 'mongoose';

export type MaintenanceDocument = Maintenance & Document;

@Schema({
  collection: 'Maintenance',
  timestamps: true,
})
export class Maintenance {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Vehicle' }) // Foreign key reference to the Vehicle model
  vehicleId: Vehicle;

  @Prop({ required: true })
  serviceDate: Date;

  @Prop({ required: true })
  serviceType: string;

  @Prop({ required: true })
  serviceProvider: string;

  @Prop({ required: true })
  serviceCost: number;

  @Prop()
  notes: string;

  @Prop({
    type: Boolean,
    default: true,
  })
  isActive: boolean;
}

export const MaintenanceSchema = SchemaFactory.createForClass(Maintenance);
