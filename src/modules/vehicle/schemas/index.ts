export { VehicleSchema, Vehicle, VehicleDocument } from './vehicle.schema';

export {
  MaintenanceSchema,
  Maintenance,
  MaintenanceDocument,
} from './maintenance.schema';

export {
  VehicleTrackingLogSchema,
  VehicleTrackingLog,
  VehicleTrackingLogDocument,
} from './vehicle-tracking-log.schema';

export {
  VehicleTrackingDataSchema,
  VehicleTrackingData,
  VehicleTrackingDataDocument,
} from './vehicle-tracking-data.schema';
