import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Types } from 'mongoose';
import { Vehicle } from './vehicle.schema';
import { PointSchema } from './point.schema';

export type VehicleTrackingLogDocument = VehicleTrackingLog & Document;

@Schema({
  collection: 'VehicleTrackingLog',
  timestamps: true,
})
export class VehicleTrackingLog {
  @Prop({ required: true })
  startDate: Date;

  @Prop({ default: null })
  endDate: Date;

  @Prop({ type: Types.ObjectId, ref: 'Vehicle', required: true })
  vehicleId: Vehicle;

  @Prop(raw(PointSchema))
  startLocation: PointSchema;

  @Prop(raw(PointSchema))
  endLocation: PointSchema;

  @Prop({ default: true })
  isActive: boolean;
}

export const VehicleTrackingLogSchema =
  SchemaFactory.createForClass(VehicleTrackingLog);
