import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Vehicle } from './vehicle.schema';
import { Types } from 'mongoose';
import { PointSchema } from './point.schema';

export type VehicleTrackingDataDocument = VehicleTrackingData & Document;

@Schema({
  collection: 'VehicleTrackingData',
  timestamps: true,
})
export class VehicleTrackingData {
  @Prop({ type: Types.ObjectId, ref: 'Vehicle', required: true, unique: true })
  vehicleId: Vehicle;

  @Prop({ required: true })
  speed: number;

  @Prop({ required: true })
  locationName: string;

  @Prop(raw(PointSchema))
  location: PointSchema;
}

export const VehicleTrackingDataSchema =
  SchemaFactory.createForClass(VehicleTrackingData);
