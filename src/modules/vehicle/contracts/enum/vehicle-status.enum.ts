export enum VehicleStatusEnum {
  ACTIVE = 'ACTIVE',
  MAINTENANCE = 'MAINTENANCE',
  RUNNING = 'RUNNING',
}
