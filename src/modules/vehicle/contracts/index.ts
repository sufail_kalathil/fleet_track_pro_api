// enum
export { VehicleStatusEnum } from './enum/vehicle-status.enum';
export { TrackingActionEnum } from './enum/tracking-action.enum';

// abstractions
export { IGeolocation } from './abstraction/geolocation';
export { ITrackingLog } from './abstraction/tracking-log';
