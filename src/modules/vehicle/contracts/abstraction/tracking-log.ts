import { IGeolocation } from './geolocation';

export interface ITrackingLog {
  vehicleId: string;
  startDate?: Date;
  endDate?: Date;
  startLocation?: IGeolocation;
  endLocation?: IGeolocation;
}
