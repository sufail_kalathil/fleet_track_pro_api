export interface IGeolocation {
  type: string;
  coordinates: number[];
}
