import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Put,
  Query,
} from '@nestjs/common';
import { VehicleService } from './vehicle.service';
import { CreateVehicleDto, UpdateVehicleDto } from './dto';
import { RequestDto } from '../../common';

@Controller('vehicle')
export class VehicleController {
  constructor(private readonly vehicleService: VehicleService) {}

  @Post()
  create(@Body() createVehicleDto: CreateVehicleDto) {
    return this.vehicleService.create(createVehicleDto);
  }

  @Get()
  findAll(@Query() request: RequestDto) {
    return this.vehicleService.findAll(request);
  }

  @Get(':id')
  findById(@Param('id') id: string) {
    return this.vehicleService.findById(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateVehicleDto: UpdateVehicleDto) {
    return this.vehicleService.update(id, updateVehicleDto);
  }

  @Patch(':id/status')
  updateVehicleStatus(
    @Param('id') id: string,
    updateVehicleDto: UpdateVehicleDto,
  ) {
    return this.vehicleService.updateVehicleStatus(id, updateVehicleDto);
  }
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.vehicleService.remove(id);
  }

  @Get(':vehicleId/analytics')
  findVehicleAnalytics(@Param('vehicleId') vehicleId: string) {
    return this.vehicleService.findVehicleAnalytics(vehicleId);
  }
}
