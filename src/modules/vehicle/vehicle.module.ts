import { Module } from '@nestjs/common';
import { VehicleService } from './vehicle.service';
import { VehicleController } from './vehicle.controller';
import { MaintenanceController } from './maintenance/maintenance.controller';
import { MaintenanceService } from './maintenance/maintenance.service';
import { MongooseModule } from '@nestjs/mongoose';
import {
  Maintenance,
  Vehicle,
  VehicleSchema,
  MaintenanceSchema,
  VehicleTrackingData,
  VehicleTrackingDataSchema,
  VehicleTrackingLog,
  VehicleTrackingLogSchema,
} from './schemas';
import {
  MaintenanceRepository,
  VehicleRepository,
  VehicleTrackingDataRepository,
  VehicleTrackingLogRepository,
} from './repository';
import { TrackingService } from './tracking/tracking.service';
import { TrackingController } from './tracking/tracking.controller';
import { TrackVehicleGateway } from '../../track-vehicle/track-vehicle.gateway';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Vehicle.name,
        schema: VehicleSchema,
      },
      {
        name: Maintenance.name,
        schema: MaintenanceSchema,
      },
      {
        name: VehicleTrackingData.name,
        schema: VehicleTrackingDataSchema,
      },
      {
        name: VehicleTrackingLog.name,
        schema: VehicleTrackingLogSchema,
      },
    ]),
  ],
  controllers: [VehicleController, MaintenanceController, TrackingController],
  providers: [
    VehicleService,
    MaintenanceService,
    TrackingService,
    VehicleRepository,
    MaintenanceRepository,
    VehicleTrackingDataRepository,
    VehicleTrackingLogRepository,
    TrackVehicleGateway,
  ],
})
export class VehicleModule {}
