import { BaseRepository } from '../../../common';
import { VehicleTrackingData, VehicleTrackingDataDocument } from '../schemas';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

export class VehicleTrackingDataRepository extends BaseRepository<VehicleTrackingDataDocument> {
  constructor(
    @InjectModel(VehicleTrackingData.name)
    vehicleTrackingDataModel: Model<VehicleTrackingDataDocument>,
  ) {
    super(vehicleTrackingDataModel);
  }
}
