import { BaseRepository } from '../../../common';
import { VehicleTrackingLog, VehicleTrackingLogDocument } from '../schemas';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

export class VehicleTrackingLogRepository extends BaseRepository<VehicleTrackingLogDocument> {
  constructor(
    @InjectModel(VehicleTrackingLog.name)
    vehicleTrackingLogModel: Model<VehicleTrackingLogDocument>,
  ) {
    super(vehicleTrackingLogModel);
  }
}
