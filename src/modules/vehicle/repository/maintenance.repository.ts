import { BaseRepository } from '../../../common/BaseRepository';
import { InjectModel } from '@nestjs/mongoose';
import { Maintenance, MaintenanceDocument } from '../schemas';
import { Model } from 'mongoose';

export class MaintenanceRepository extends BaseRepository<MaintenanceDocument> {
  constructor(
    @InjectModel(Maintenance.name)
    maintenanceModel: Model<MaintenanceDocument>,
  ) {
    super(maintenanceModel);
  }
}
