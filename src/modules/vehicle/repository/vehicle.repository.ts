import { BaseRepository } from '../../../common/BaseRepository';
import { InjectModel } from '@nestjs/mongoose';
import { VehicleDocument, Vehicle } from '../schemas';
import { Model } from 'mongoose';

export class VehicleRepository extends BaseRepository<VehicleDocument> {
  constructor(@InjectModel(Vehicle.name) vehicleModel: Model<VehicleDocument>) {
    super(vehicleModel);
  }
}
