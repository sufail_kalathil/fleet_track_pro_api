import { Test, TestingModule } from '@nestjs/testing';
import { MaintenanceController } from './maintenance.controller';
import { CreateVehicleDto, UpdateVehicleDto } from '../dto';
import { VehicleStatusEnum } from '../contracts';
import { RequestDto, SortByEnum, SortOrderEnum } from '../../../common';
import { VehicleService } from '../vehicle.service';
import { MaintenanceService } from './maintenance.service';

describe('MaintenanceController', () => {
  let controller: MaintenanceController;

  const mockFunctions = {
    create: jest.fn(),
    findAll: jest.fn(),
    findById: jest.fn(),
    update: jest.fn(),
    remove: jest.fn(),
  };

  const createRequest: any = {
    vehicleId: '65cbe00e24f82957c88337ab',
    serviceDate: new Date(),
    serviceType: 'Oil Change',
    serviceProvider: 'XYZ Auto Shop',
    serviceCost: 20,
    notes: 'Regular scheduled maintenance.',
  };

  const updateRequest: any = {
    vehicleId: '65cbe00e24f82957c88337ab',
    serviceDate: new Date(),
    serviceType: 'Oil Change',
    serviceProvider: 'XYZ Auto Shop',
    serviceCost: 20,
    notes: 'Regular scheduled maintenance.',
  };

  const maintenances = [
    {
      vehicleId: '65cbe00e24f82957c88337ab',
      serviceDate: new Date(),
      serviceType: 'Oil Change',
      serviceProvider: 'XYZ Auto Shop',
      serviceCost: 20,
      notes: 'Regular scheduled maintenance.',
    },
    {
      vehicleId: '65cbe00e24f82957c88337ab',
      serviceDate: new Date(),
      serviceType: 'Oil Change',
      serviceProvider: 'XYZ Auto Shop',
      serviceCost: 20,
      notes: 'Regular scheduled maintenance.',
    },
  ];

  const listRequest: RequestDto = {
    paginate: false,
    pageSize: 1,
    pageNo: 10,
    sortOrder: SortOrderEnum.ASC,
    sortBy: SortByEnum.CREATED_AT,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MaintenanceController],
      providers: [
        {
          provide: MaintenanceService,
          useValue: mockFunctions,
        },
      ],
    }).compile();

    controller = module.get<MaintenanceController>(MaintenanceController);
  });

  it('Should create maintenance', async () => {
    jest.spyOn(mockFunctions, 'create').mockReturnValue(maintenances[0]);
    const result = await controller.create(createRequest);

    expect(result).toBe(maintenances[0]);
    expect(mockFunctions.create).toHaveBeenCalled();
  });

  it('Should list all maintenance', async () => {
    jest.spyOn(mockFunctions, 'findAll').mockReturnValue(maintenances);
    const result = await controller.findAll(listRequest);

    expect(result).toBe(maintenances);
    expect(mockFunctions.findAll).toHaveBeenCalled();
  });

  it('Should list single maintenance', async () => {
    jest.spyOn(mockFunctions, 'findById').mockReturnValue(maintenances[0]);
    const result = await controller.findById('dsd88787');

    expect(result).toBe(maintenances[0]);
    expect(mockFunctions.findById).toHaveBeenCalled();
  });

  it('Should delete maintenance', async () => {
    jest.spyOn(mockFunctions, 'remove').mockReturnValue(undefined);
    const result = await controller.remove('dsd88787');

    expect(result).toBe(undefined);
    expect(mockFunctions.remove).toHaveBeenCalled();
  });

  it('Should update maintenance', async () => {
    jest.spyOn(mockFunctions, 'update').mockReturnValue(undefined);
    const result = await controller.update('dsd88787', updateRequest);

    expect(result).toBe(undefined);
    expect(mockFunctions.update).toHaveBeenCalled();
  });
});
