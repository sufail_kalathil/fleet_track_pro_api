import { flatten, Injectable, NotFoundException } from '@nestjs/common';
import { MaintenanceRepository } from '../repository';
import { CreateMaintenanceDto } from '../dto';
import { exceptionMessage } from '../constants';
import { IPopulate, RequestDto, responseTransformer } from '../../../common';

@Injectable()
export class MaintenanceService {
  constructor(private readonly maintenanceRepository: MaintenanceRepository) {}

  create(createMaintenanceDto: CreateMaintenanceDto) {
    return this.maintenanceRepository.create(createMaintenanceDto);
  }
  async findAll(request: RequestDto) {
    const { filter, options } =
      this.maintenanceRepository.BasicQueryBuilder(request);

    const populate: IPopulate = {
      path: 'vehicleId',
      select: '_id brand model uin',
    };
    const [count, vehicles] = await Promise.all([
      this.maintenanceRepository.countDocuments(filter),
      this.maintenanceRepository.findAll(filter, options, populate),
    ]);
    return responseTransformer(request, vehicles, count);
  }

  findMaintenanceByVehicle(vehicleId: string) {
    const query = { vehicleId, isActive: true };
    return this.maintenanceRepository.findAll(query);
  }

  async findById(_id: string) {
    const maintenance = await this.maintenanceRepository.findOne({
      _id,
      isActive: true,
    });

    if (!maintenance) {
      throw new NotFoundException(`${exceptionMessage.MAINTENANCE_NOT_FOUND}`);
    }

    return maintenance;
  }

  async update(id: string, data: any): Promise<void> {
    const maintenance = await this.maintenanceRepository.update(id, data);

    if (!maintenance) {
      throw new NotFoundException(`${exceptionMessage.VEHICLE_NOT_FOUND}`);
    }

    return;
  }

  async remove(id: string): Promise<void> {
    const maintenance = await this.maintenanceRepository.update(id, {
      isActive: false,
    });

    if (!maintenance) {
      throw new NotFoundException(`${exceptionMessage.MAINTENANCE_NOT_FOUND}`);
    }

    return;
  }

  findMaintenanceCostByVehicle(vehicleId: string) {
    const stages = [
      {
        $match: { vehicleId },
      },
      {
        $group: {
          _id: '$vehicleId',
          totalCost: { $sum: '$serviceCost' },
        },
      },
      {
        $project: {
          _id: 0,
          totalCost: 1,
        },
      },
    ];
    return this.maintenanceRepository.aggregate(stages);
  }
}
