import { Test, TestingModule } from '@nestjs/testing';
import { MaintenanceService } from './maintenance.service';
import { CreateMaintenanceDto } from '../dto';
import { RequestDto, SortByEnum, SortOrderEnum } from '../../../common';
import { NotFoundException } from '@nestjs/common';
import { MaintenanceController } from './maintenance.controller';
import { MaintenanceRepository } from '../repository';

describe('MaintenanceService', () => {
  let service: MaintenanceService;
  const repositoryMockFunctions = {
    findById: jest.fn(),
    find: jest.fn(),
    findOne: jest.fn(),
    findAll: jest.fn(),
    create: jest.fn(),
    update: jest.fn(),
    countDocuments: jest.fn(),
    aggregate: jest.fn(),
    BasicQueryBuilder: jest.fn(),
  };

  const mockQueryBuilder = {
    filter: { isActive: true },
    options: {
      limit: 10,
      skip: 1,
    },
  };

  const createRequest: any = {
    vehicleId: '65cbe00e24f82957c88337ab',
    serviceDate: new Date(),
    serviceType: 'Oil Change',
    serviceProvider: 'XYZ Auto Shop',
    serviceCost: 20,
    notes: 'Regular scheduled maintenance.',
  };

  const updateRequest: any = {
    vehicleId: '65cbe00e24f82957c88337ab',
    serviceDate: new Date(),
    serviceType: 'Oil Change',
    serviceProvider: 'XYZ Auto Shop',
    serviceCost: 20,
    notes: 'Regular scheduled maintenance.',
  };

  const listRequest: RequestDto = {
    paginate: false,
    pageSize: 1,
    pageNo: 10,
    sortOrder: SortOrderEnum.ASC,
    sortBy: SortByEnum.CREATED_AT,
  };

  const maintenances = [
    {
      vehicleId: '65cbe00e24f82957c88337ab',
      serviceDate: new Date(),
      serviceType: 'Oil Change',
      serviceProvider: 'XYZ Auto Shop',
      serviceCost: 20,
      notes: 'Regular scheduled maintenance.',
    },
    {
      vehicleId: '65cbe00e24f82957c88337ab',
      serviceDate: new Date(),
      serviceType: 'Oil Change',
      serviceProvider: 'XYZ Auto Shop',
      serviceCost: 20,
      notes: 'Regular scheduled maintenance.',
    },
  ];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MaintenanceService,
        {
          provide: MaintenanceRepository,
          useValue: repositoryMockFunctions,
        },
      ],
    }).compile();

    service = module.get<MaintenanceService>(MaintenanceService);
  });

  describe('Create maintenance', () => {
    it('Should create  maintenance ', async () => {
      const newMaintenance = { _id: 'sdsd' };
      jest
        .spyOn(repositoryMockFunctions, 'create')
        .mockReturnValue(newMaintenance);
      const result = await service.create(createRequest);

      expect(result).toBe(newMaintenance);
      expect(repositoryMockFunctions.create).toHaveBeenCalled();
    });
  });

  describe('List Maintenance', () => {
    it('Should list the maintenance', async () => {
      jest
        .spyOn(repositoryMockFunctions, 'BasicQueryBuilder')
        .mockReturnValue(mockQueryBuilder);

      jest.spyOn(repositoryMockFunctions, 'countDocuments').mockReturnValue(5);
      jest
        .spyOn(repositoryMockFunctions, 'findAll')
        .mockReturnValue(maintenances);

      const result = await service.findAll(listRequest);

      expect(repositoryMockFunctions.BasicQueryBuilder).toBeDefined();
      expect(repositoryMockFunctions.countDocuments).toBeDefined();
      expect(repositoryMockFunctions.findAll).toBeDefined();
      expect(result).toBe(maintenances);
    });

    it('Should return error if no maintenance found', async () => {
      try {
        jest
          .spyOn(repositoryMockFunctions, 'findOne')
          .mockReturnValue(undefined);
        const result = await service.findById('dfdiiiq');
      } catch (e) {
        expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
        expect(e).toBeInstanceOf(NotFoundException);
      }
    });

    it('Should return maintenance by id', async () => {
      jest
        .spyOn(repositoryMockFunctions, 'findOne')
        .mockReturnValue(maintenances[0]);
      const result = await service.findById('dfdiiiq');

      expect(result).toBe(maintenances[0]);
      expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
    });
  });

  describe('Update maintenance', () => {
    it('Should return error if maintenance not found', async () => {
      try {
        jest
          .spyOn(repositoryMockFunctions, 'update')
          .mockReturnValue(undefined);
        const result = await service.update('id65d', updateRequest);
      } catch (e) {
        expect(repositoryMockFunctions.update).toHaveBeenCalled();
        expect(e).toBeInstanceOf(NotFoundException);
      }
    });

    it('update maintenance', async () => {
      jest
        .spyOn(repositoryMockFunctions, 'update')
        .mockReturnValue(maintenances[0]);
      const result = await service.update('id65d', updateRequest);

      expect(result).toBe(undefined);
      expect(repositoryMockFunctions.update).toHaveBeenCalled();
    });
  });

  describe('Remove maintenance', () => {
    it('Should return error if maintenance not found', async () => {
      try {
        jest
          .spyOn(repositoryMockFunctions, 'update')
          .mockReturnValue(undefined);
        const result = await service.remove('id65d');
      } catch (e) {
        expect(repositoryMockFunctions.update).toHaveBeenCalled();
        expect(e).toBeInstanceOf(NotFoundException);
      }
    });

    it('Remove maintenance', async () => {
      jest
        .spyOn(repositoryMockFunctions, 'update')
        .mockReturnValue(maintenances[0]);
      const result = await service.remove('id65d');

      expect(result).toBe(undefined);
      expect(repositoryMockFunctions.update).toHaveBeenCalled();
    });
  });
});
