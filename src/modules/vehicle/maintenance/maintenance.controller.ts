import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { MaintenanceService } from './maintenance.service';
import { CreateMaintenanceDto, UpdateMaintenanceDto } from '../dto';
import { RequestDto } from '../../../common';

@Controller('maintenance')
export class MaintenanceController {
  constructor(private readonly maintenanceService: MaintenanceService) {}

  @Post()
  create(@Body() createMaintenanceDto: CreateMaintenanceDto) {
    return this.maintenanceService.create(createMaintenanceDto);
  }
  @Get()
  findAll(@Query() request: RequestDto) {
    return this.maintenanceService.findAll(request);
  }

  @Get('vehicle/:vehicleId')
  findMaintenanceByVehicle(@Param('vehicleId') vehicleId: string) {
    return this.maintenanceService.findMaintenanceByVehicle(vehicleId);
  }

  @Get('analytics/vehicle/:vehicleId/cost')
  findMaintenanceCostByVehicle(@Param('vehicleId') vehicleId: string) {
    return this.maintenanceService.findMaintenanceCostByVehicle(vehicleId);
  }

  @Get(':id')
  findById(@Param('id') id: string) {
    return this.maintenanceService.findById(id);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateMaintenanceDto: UpdateMaintenanceDto,
  ) {
    return this.maintenanceService.update(id, updateMaintenanceDto);
  }
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.maintenanceService.remove(id);
  }
}
