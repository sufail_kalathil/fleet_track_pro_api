import { CreateMaintenanceDto } from './create-maintenance.dto';
import { PartialType } from '@nestjs/mapped-types';

export class UpdateMaintenanceDto extends PartialType(CreateMaintenanceDto) {}
