import { PartialType } from '@nestjs/mapped-types';
import { TrackingDto } from './tracking.dto';
import { IsNotEmpty, IsNumber } from 'class-validator';
import { trackingErrorMessage } from '../constants/error-message';

export class TrackingLogDto extends PartialType(TrackingDto) {
  @IsNotEmpty({ message: trackingErrorMessage.speed.required })
  @IsNumber({}, { message: trackingErrorMessage.speed.type })
  speed: number;
}
