import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { trackingErrorMessage } from '../constants/error-message';

export class TrackingDto {
  @IsNotEmpty({ message: trackingErrorMessage.locationName.required })
  @IsString({ message: trackingErrorMessage.locationName.required })
  locationName: string;

  @IsNotEmpty({ message: trackingErrorMessage.latitude.required })
  @IsNumber({}, { message: trackingErrorMessage.latitude.required })
  latitude: number;

  @IsNotEmpty({ message: trackingErrorMessage.longitude.required })
  @IsNumber({}, { message: trackingErrorMessage.longitude.required })
  longitude: number;
}
