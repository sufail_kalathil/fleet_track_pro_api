import {
  IsDate,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { Types } from 'mongoose';
import { maintenanceErrorMessage } from '../constants/error-message';
import { Transform } from 'class-transformer';

export class CreateMaintenanceDto {
  @IsNotEmpty({ message: maintenanceErrorMessage.vehicle.required })
  vehicleId: Types.ObjectId;

  @IsNotEmpty({ message: maintenanceErrorMessage.serviceDate.required })
  @Transform(({ value }) => new Date(value))
  @IsDate({ message: maintenanceErrorMessage.serviceDate.type })
  serviceDate: Date;

  @IsNotEmpty({ message: maintenanceErrorMessage.serviceType.required })
  @IsString({ message: maintenanceErrorMessage.serviceType.type })
  serviceType: string;

  @IsNotEmpty({ message: maintenanceErrorMessage.serviceProvider.required })
  @IsString({ message: maintenanceErrorMessage.serviceProvider.type })
  serviceProvider: string;

  @IsNotEmpty({ message: maintenanceErrorMessage.serviceCost.required })
  @Transform(({ value }) => (value ? Number(value) : ''))
  @IsNumber({}, { message: maintenanceErrorMessage.serviceCost.type })
  serviceCost: number;

  @IsOptional()
  @IsString({ message: maintenanceErrorMessage.notes.type })
  notes?: string;
}
