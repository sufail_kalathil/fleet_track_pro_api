import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { errorMessage } from '../constants';
import { Transform } from 'class-transformer';

export class CreateVehicleDto {
  @IsNotEmpty({ message: errorMessage.brand.required })
  @IsString({ message: errorMessage.brand.type })
  readonly brand: string;

  @IsNotEmpty({ message: errorMessage.model.required })
  @IsString({ message: errorMessage.model.type })
  readonly model: string;

  @IsNotEmpty({ message: errorMessage.year.required })
  @Transform(({ value }) => (value ? Number(value) : ''))
  @IsNumber({}, { message: errorMessage.year.type })
  readonly year: number;

  @IsNotEmpty({ message: errorMessage.uin.required })
  @IsString({ message: errorMessage.uin.type })
  readonly uin: string;

  @IsOptional()
  @IsString({ message: errorMessage.color.type })
  readonly color?: string;

  @IsOptional()
  @IsString({ message: errorMessage.type.type })
  readonly type?: string;

  @IsOptional()
  @IsString({ message: errorMessage.engineNumber.type })
  readonly engineNumber?: string;
}
