export const exceptionMessage = {
  VEHICLE_NOT_FOUND: 'vehicle not found',
  MAINTENANCE_NOT_FOUND: 'maintenance record not found',
  VEHICLE_ALREADY_RUNNING: 'vehicle is already running',
  VEHICLE_NOT_AVAILABLE: 'vehicle not available due to maintenance',
  VEHICLE_NOT_STARTED: 'vehicle not started yet',
};
