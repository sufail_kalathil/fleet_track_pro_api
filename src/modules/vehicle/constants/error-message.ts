export const errorMessage = {
  brand: {
    required: 'brand is required',
    type: 'invalid brand',
  },
  model: {
    required: 'model is required',
    type: 'invalid model',
  },
  year: {
    required: 'year is required',
    type: 'invalid year',
  },
  uin: {
    required: 'uin is required',
    type: 'invalid uin',
  },
  color: {
    type: 'invalid color',
  },
  engineNumber: {
    type: 'invalid engine number',
  },
  type: {
    type: 'invalid type',
  },
};

export const maintenanceErrorMessage = {
  vehicle: {
    required: 'vehicle is required',
    type: 'invalid vehicle',
  },
  serviceDate: {
    required: 'serviceDate is required',
    type: 'invalid serviceDate',
  },
  serviceType: {
    required: 'serviceType is required',
    type: 'invalid serviceType',
  },
  serviceProvider: {
    required: 'serviceProvider is required',
    type: 'invalid serviceProvider',
  },
  serviceCost: {
    required: 'serviceCost is required',
    type: 'invalid serviceCost',
  },
  notes: {
    type: 'invalid notes',
  },
};

export const trackingErrorMessage = {
  locationName: {
    required: 'locationName is required',
    type: 'invalid locationName',
  },
  latitude: {
    required: 'latitude is required',
    type: 'invalid latitude',
  },
  longitude: {
    required: 'longitude is required',
    type: 'invalid longitude',
  },
  speed: {
    required: 'speed is required',
    type: 'invalid speed',
  },
};
