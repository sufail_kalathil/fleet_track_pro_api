import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import {
  VehicleRepository,
  VehicleTrackingDataRepository,
  VehicleTrackingLogRepository,
} from '../repository';
import { exceptionMessage } from '../constants';
import {
  IGeolocation,
  ITrackingLog,
  TrackingActionEnum,
  VehicleStatusEnum,
} from '../contracts';
import { Connection } from 'mongoose';
import { InjectConnection } from '@nestjs/mongoose';
import { TrackingDto, TrackingLogDto } from '../dto';
import { TrackVehicleGateway } from '../../../track-vehicle/track-vehicle.gateway';

@Injectable()
export class TrackingService {
  constructor(
    private readonly vehicleRepository: VehicleRepository,
    private readonly vehicleTrackingDataRepository: VehicleTrackingDataRepository,
    private readonly vehicleTrackingLogRepository: VehicleTrackingLogRepository,
    private readonly trackVehicleGateway: TrackVehicleGateway,
  ) {}

  /**
   * To find the vehicle details by its unique id.
   * @param _id - unique id of the vehicle.
   */
  async findVehicleById(_id: string) {
    const vehicle = await this.vehicleRepository.findOne({
      _id,
      isActive: true,
    });

    if (!vehicle) {
      throw new NotFoundException(`${exceptionMessage.VEHICLE_NOT_FOUND}`);
    }
    return vehicle;
  }

  /**
   * This function triggers the start of the tracking process for the specified vehicle.
   * It accepts a unique identifier for the vehicle and a tracking request object.
   * @param vehicleId - The unique identifier of the vehicle to be tracked.
   * @param trackingDto - The tracking request containing necessary data for tracking.
   */
  async startTracking(vehicleId: string, trackingDto: TrackingDto) {
    const vehicle = await this.findVehicleById(vehicleId);

    if (vehicle.status === VehicleStatusEnum.RUNNING) {
      throw new BadRequestException(
        `${exceptionMessage.VEHICLE_ALREADY_RUNNING}`,
      );
    }

    if (vehicle.status === VehicleStatusEnum.MAINTENANCE) {
      throw new BadRequestException(
        `${exceptionMessage.VEHICLE_NOT_AVAILABLE}`,
      );
    }

    const { locationName, latitude, longitude } = trackingDto;
    const locationData = {
      type: 'Point',
      coordinates: [longitude, latitude],
    };

    const trackingOperations: Array<Promise<any>> = [
      this.insertOrUpdateTrackingData(vehicleId, 0, locationName, locationData),
      this.insertOrUpdateTrackingLog(vehicleId, locationData),
      this.updateVehicleStatus(vehicleId, VehicleStatusEnum.RUNNING),
    ];

    await Promise.all(trackingOperations);

    const dataToSync = {
      vehicleId,
      status: VehicleStatusEnum.RUNNING,
      locationName,
      latitude,
      longitude,
    };
    this.trackVehicleGateway.syncStartEvent(JSON.stringify(dataToSync));
  }

  /**
   * This function halt tracking process for the specified vehicle.
   * It accepts a unique identifier for the vehicle and a tracking request object.
   * @param vehicleId - The unique identifier of the vehicle to be tracked.
   * @param trackingDto - The tracking request containing necessary data for tracking.
   */
  async stopTracking(vehicleId: string, trackingDto: TrackingDto) {
    const vehicle = await this.findVehicleById(vehicleId);

    if (vehicle.status !== VehicleStatusEnum.RUNNING) {
      throw new BadRequestException(`${exceptionMessage.VEHICLE_NOT_STARTED}`);
    }

    const { locationName, latitude, longitude } = trackingDto;
    const locationData: IGeolocation = {
      type: 'Point',
      coordinates: [longitude, latitude],
    };

    const trackingOperations: Array<Promise<any>> = [
      this.insertOrUpdateTrackingData(vehicleId, 0, locationName, locationData),
      this.insertOrUpdateTrackingLog(
        vehicleId,
        locationData,
        TrackingActionEnum.STOP,
      ),
      this.updateVehicleStatus(vehicleId, VehicleStatusEnum.ACTIVE),
    ];

    await Promise.all(trackingOperations);

    const dataToSync = {
      vehicleId,
      status: VehicleStatusEnum.ACTIVE,
      locationName,
      latitude,
      longitude,
    };
    this.trackVehicleGateway.syncStopEvent(JSON.stringify(dataToSync));
  }

  /**
   * This function triggers the start of logging activity for the specified vehicle
   * @param vehicleId -The unique identifier of the vehicle to be tracked.
   * @param trackingLogDto - The data object containing information relevant to the logging process.
   */
  async logTracking(vehicleId: string, trackingLogDto: TrackingLogDto) {
    const vehicle = await this.findVehicleById(vehicleId);

    if (vehicle.status !== VehicleStatusEnum.RUNNING) {
      throw new BadRequestException(`${exceptionMessage.VEHICLE_NOT_STARTED}`);
    }

    const { locationName, latitude, longitude, speed } = trackingLogDto;
    const locationData: IGeolocation = {
      type: 'Point',
      coordinates: [longitude, latitude],
    };

    // upsert data in tracking data table
    await this.insertOrUpdateTrackingData(
      vehicleId,
      speed,
      locationName,
      locationData,
    );

    const liveData = await this.findVehicleLiveData(vehicleId);
    if (!liveData) {
      return;
    }
    this.trackVehicleGateway.syncLogEvent(liveData as string);
  }

  async findVehicleLiveData(vehicleId: string): Promise<string | boolean> {
    const query = { vehicleId };
    const projection = { _id: 0, _v: 0, createdAt: 0 };
    const liveData = await this.vehicleTrackingDataRepository.findOne(
      query,
      projection,
    );
    if (!liveData) {
      return false;
    }

    const { location, locationName, speed } = liveData;
    const [longitude, latitude] = location.coordinates;
    const formatData = {
      vehicleId,
      speed,
      locationName,
      longitude,
      latitude,
    };

    return JSON.stringify(formatData);
  }

  /**
   * To insert or update data in the vehicle tracking model, allowing for
   * real-time data sync.
   * @param vehicleId -id of the vehicle to update/insert.
   * @param location - vehicle location.
   * @param action - action to be taken.
   * @private
   */
  private insertOrUpdateTrackingLog(
    vehicleId: string,
    location: IGeolocation,
    action: TrackingActionEnum = TrackingActionEnum.START,
  ) {
    const trackingLogData: ITrackingLog = {
      vehicleId,
    };

    if (action === TrackingActionEnum.START) {
      trackingLogData.startDate = new Date();
      trackingLogData.startLocation = location;
      trackingLogData.endLocation = location;
      return this.vehicleTrackingLogRepository.create(trackingLogData);
    } else if (action === TrackingActionEnum.STOP) {
      trackingLogData.endDate = new Date();
      trackingLogData.endLocation = location;
      const filter = {
        vehicleId,
        endDate: null,
      };
      return this.vehicleTrackingLogRepository.upsert(filter, trackingLogData);
    }
  }

  /**
   * To insert or update data in the vehicle tracking model, allowing for
   * real-time data sync.
   * @param vehicleId - id of the vehicle to update.
   * @param speed - current speed of the vehicle.
   * @param locationName -current location name.
   * @param location - location  of the vehicle.
   * @private
   */
  private insertOrUpdateTrackingData(
    vehicleId: string,
    speed: number,
    locationName: string,
    location: IGeolocation,
  ) {
    const trackingData = {
      vehicleId,
      speed,
      locationName,
      location,
    };

    return this.vehicleTrackingDataRepository.upsert(
      { vehicleId },
      trackingData,
    );
  }

  /**
   * To update vehicle status in the vehicle model.
   * @param vehicleId - id of the vehicle
   * @param status - status to be assigned to the vehicle.
   * @private
   */
  private updateVehicleStatus(vehicleId: string, status: VehicleStatusEnum) {
    return this.vehicleRepository.update(vehicleId, { status });
  }
}
