import { Test, TestingModule } from '@nestjs/testing';
import { TrackingService } from './tracking.service';
import {
  VehicleRepository,
  VehicleTrackingDataRepository,
  VehicleTrackingLogRepository,
} from '../repository';
import { TrackVehicleGateway } from '../../../track-vehicle/track-vehicle.gateway';
import { BadRequestException, NotFoundException } from '@nestjs/common';

describe('TrackingService', () => {
  let service: TrackingService;

  const repositoryMockFunctions = {
    findById: jest.fn(),
    find: jest.fn(),
    findOne: jest.fn(),
    findAll: jest.fn(),
    upsert: jest.fn(),
    create: jest.fn(),
    update: jest.fn(),
    countDocuments: jest.fn(),
    aggregate: jest.fn(),
    BasicQueryBuilder: jest.fn(),
  };

  const gateWayMockFunction = {
    syncStartEvent: jest.fn(),
    syncStopEvent: jest.fn(),
    syncLogEvent: jest.fn(),
  };

  const trackingRequest = {
    locationName: 'start locaion',
    latitude: -12.856077,
    longitude: 40.848447,
  };

  const trackingLogRequest = {
    locationName: 'logg location',
    latitude: -10.856077,
    longitude: 20.848447,
    speed: 10,
  };

  const activeVehicle = {
    brand: 'Toyota',
    model: 'Innoca',
    year: 2023,
    uin: '12121',
    color: 'Orange',
    type: 'Sedan',
    engineNumber: 'ABC123456',
    status: 'ACTIVE',
  };

  const runningVehicle = {
    brand: 'Toyota',
    model: 'Innoca',
    year: 2023,
    uin: '12121',
    color: 'Orange',
    type: 'Sedan',
    engineNumber: 'ABC123456',
    status: 'RUNNING',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TrackingService,
        {
          provide: VehicleRepository,
          useValue: repositoryMockFunctions,
        },
        {
          provide: VehicleTrackingDataRepository,
          useValue: repositoryMockFunctions,
        },
        {
          provide: VehicleTrackingLogRepository,
          useValue: repositoryMockFunctions,
        },
        {
          provide: TrackVehicleGateway,
          useValue: gateWayMockFunction,
        },
      ],
    }).compile();

    service = module.get<TrackingService>(TrackingService);
  });

  describe('Track Start Event', () => {
    it('Throw if vehicle not found', async () => {
      try {
        jest
          .spyOn(repositoryMockFunctions, 'findOne')
          .mockReturnValue(undefined);
        const result = await service.startTracking('sdsd', trackingRequest);
      } catch (e) {
        expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
        expect(e).toBeInstanceOf(NotFoundException);
      }
    });

    it('Throw error if not in active status', async () => {
      try {
        jest
          .spyOn(repositoryMockFunctions, 'findOne')
          .mockReturnValue(runningVehicle);
        const result = await service.startTracking('sdsd', trackingRequest);
      } catch (e) {
        expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
        expect(e).toBeInstanceOf(BadRequestException);
      }
    });

    it('Log Start Event', async () => {
      jest
        .spyOn(repositoryMockFunctions, 'findOne')
        .mockReturnValue(activeVehicle);

      jest.spyOn(repositoryMockFunctions, 'upsert').mockReturnValue({});
      jest.spyOn(repositoryMockFunctions, 'create').mockReturnValue({});
      jest.spyOn(repositoryMockFunctions, 'update').mockReturnValue({});
      jest.spyOn(gateWayMockFunction, 'syncStartEvent').mockReturnValue({});

      const result = await service.startTracking('sdsd', trackingRequest);

      expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
      expect(repositoryMockFunctions.upsert).toHaveBeenCalled();
      expect(repositoryMockFunctions.create).toHaveBeenCalled();
      expect(repositoryMockFunctions.update).toHaveBeenCalled();
      expect(gateWayMockFunction.syncStartEvent).toHaveBeenCalled();
      expect(result).toBe(undefined);
    });
  });

  describe('Track Stop Event', () => {
    it('Throw if vehicle not found', async () => {
      try {
        jest
          .spyOn(repositoryMockFunctions, 'findOne')
          .mockReturnValue(undefined);
        const result = await service.stopTracking('sdsd', trackingRequest);
      } catch (e) {
        expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
        expect(e).toBeInstanceOf(NotFoundException);
      }
    });

    it('Throw error if not in running status', async () => {
      try {
        jest
          .spyOn(repositoryMockFunctions, 'findOne')
          .mockReturnValue(activeVehicle);
        const result = await service.stopTracking('sdsd', trackingRequest);
      } catch (e) {
        expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
        expect(e).toBeInstanceOf(BadRequestException);
      }
    });

    it('Log Stop Event', async () => {
      jest
        .spyOn(repositoryMockFunctions, 'findOne')
        .mockReturnValue(runningVehicle);

      jest.spyOn(repositoryMockFunctions, 'upsert').mockReturnValue({});
      jest.spyOn(repositoryMockFunctions, 'upsert').mockReturnValue({});
      jest.spyOn(repositoryMockFunctions, 'update').mockReturnValue({});
      jest.spyOn(gateWayMockFunction, 'syncStopEvent').mockReturnValue({});

      const result = await service.stopTracking('sdsd', trackingRequest);

      expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
      expect(repositoryMockFunctions.upsert).toHaveBeenCalled();
      expect(repositoryMockFunctions.update).toHaveBeenCalled();
      expect(gateWayMockFunction.syncStopEvent).toHaveBeenCalled();
      expect(result).toBe(undefined);
    });
  });

  describe('Track Log Event', () => {
    it('Throw if vehicle not found', async () => {
      try {
        jest
          .spyOn(repositoryMockFunctions, 'findOne')
          .mockReturnValue(undefined);
        const result = await service.logTracking('sdsd', trackingLogRequest);
      } catch (e) {
        expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
        expect(e).toBeInstanceOf(NotFoundException);
      }
    });

    it('Throw error if not in running status', async () => {
      try {
        jest
          .spyOn(repositoryMockFunctions, 'findOne')
          .mockReturnValue(activeVehicle);
        const result = await service.logTracking('sdsd', trackingLogRequest);
      } catch (e) {
        expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
        expect(e).toBeInstanceOf(BadRequestException);
      }
    });

    it('Log Event', async () => {
      jest
        .spyOn(repositoryMockFunctions, 'findOne')
        .mockReturnValue(runningVehicle);

      jest.spyOn(repositoryMockFunctions, 'upsert').mockReturnValue({});
      jest
        .spyOn(repositoryMockFunctions, 'findOne')
        .mockReturnValue({
          ...runningVehicle,
          location: { coordinates: [0, 0] },
        });
      jest.spyOn(gateWayMockFunction, 'syncLogEvent').mockReturnValue({});

      const result = await service.logTracking('sdsd', trackingLogRequest);

      expect(repositoryMockFunctions.findOne).toHaveBeenCalled();
      expect(repositoryMockFunctions.upsert).toHaveBeenCalled();
      expect(gateWayMockFunction.syncLogEvent).toHaveBeenCalled();
      expect(result).toBe(undefined);
    });
  });
});
