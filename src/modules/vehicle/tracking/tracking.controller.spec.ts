import { Test, TestingModule } from '@nestjs/testing';
import { TrackingController } from './tracking.controller';
import { TrackingService } from './tracking.service';

describe('TrackingController', () => {
  let controller: TrackingController;
  const mockFunctions = {
    startTracking: jest.fn(),
    stopTracking: jest.fn(),
    logTracking: jest.fn(),
  };

  const trackingRequest = {
    locationName: 'start locaion',
    latitude: -12.856077,
    longitude: 40.848447,
  };

  const trackingLogRequest = {
    locationName: 'logg location',
    latitude: -10.856077,
    longitude: 20.848447,
    speed: 10,
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TrackingController],
      providers: [
        {
          provide: TrackingService,
          useValue: mockFunctions,
        },
      ],
    }).compile();

    controller = module.get<TrackingController>(TrackingController);
  });

  it('Start tracking', async () => {
    jest.spyOn(mockFunctions, 'startTracking').mockReturnValue(undefined);
    const result = await controller.startTracking('dsds898', trackingRequest);

    expect(result).toBe(undefined);
    expect(mockFunctions.startTracking).toHaveBeenCalled();
  });

  it('Stop tracking', async () => {
    jest.spyOn(mockFunctions, 'stopTracking').mockReturnValue(undefined);
    const result = await controller.stopTracking('dsds898', trackingRequest);

    expect(result).toBe(undefined);
    expect(mockFunctions.stopTracking).toHaveBeenCalled();
  });

  it('Log tracking', async () => {
    jest.spyOn(mockFunctions, 'startTracking').mockReturnValue(undefined);
    const result = await controller.logTracking('dsds898', trackingLogRequest);

    expect(result).toBe(undefined);
    expect(mockFunctions.logTracking).toHaveBeenCalled();
  });
});
