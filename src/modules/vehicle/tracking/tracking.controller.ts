import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Param,
  Post,
} from '@nestjs/common';
import { TrackingService } from './tracking.service';
import { TrackingDto, TrackingLogDto } from '../dto';

@Controller('tracking')
export class TrackingController {
  constructor(private readonly trackingService: TrackingService) {}

  @HttpCode(HttpStatus.OK)
  @Post('vehicle/:vehicleId/start')
  startTracking(
    @Param('vehicleId') vehicleId: string,
    @Body() trackingDto: TrackingDto,
  ) {
    return this.trackingService.startTracking(vehicleId, trackingDto);
  }

  @HttpCode(HttpStatus.OK)
  @Post('vehicle/:vehicleId/stop')
  stopTracking(
    @Param('vehicleId') vehicleId: string,
    @Body() trackingDto: TrackingDto,
  ) {
    return this.trackingService.stopTracking(vehicleId, trackingDto);
  }

  @HttpCode(HttpStatus.OK)
  @Post('vehicle/:vehicleId/log')
  logTracking(
    @Param('vehicleId') vehicleId: string,
    @Body() trackingLogDto: TrackingLogDto,
  ) {
    return this.trackingService.logTracking(vehicleId, trackingLogDto);
  }
}
