import { Test, TestingModule } from '@nestjs/testing';
import { VehicleController } from './vehicle.controller';
import { VehicleService } from './vehicle.service';
import { CreateVehicleDto, UpdateVehicleDto } from './dto';
import { VehicleStatusEnum } from './contracts';
import { RequestDto, SortByEnum, SortOrderEnum } from '../../common';
import mock = jest.mock;

describe('VehicleController', () => {
  let controller: VehicleController;

  const mockFunctions = {
    create: jest.fn(),
    findAll: jest.fn(),
    findById: jest.fn(),
    update: jest.fn(),
    remove: jest.fn(),
  };

  const createRequest: CreateVehicleDto = {
    brand: 'Toyota',
    model: 'Innoca',
    year: 2023,
    uin: '12121',
    color: 'Orange',
    type: 'Sedan',
    engineNumber: 'ABC123456',
  };

  const updateRequest: UpdateVehicleDto = {
    brand: 'Toyota',
    model: 'Innoca',
    year: 2023,
    uin: '12121',
    color: 'Orange',
    type: 'Sedan',
    engineNumber: 'ABC123456',
    status: VehicleStatusEnum.RUNNING,
  };

  const vehicles = [
    {
      brand: 'Toyota',
      model: 'Innoca',
      year: 2023,
      uin: '12121',
      color: 'Orange',
      type: 'Sedan',
      engineNumber: 'ABC123456',
    },
    {
      brand: 'Toyota',
      model: 'Innoca',
      year: 2023,
      uin: '12121',
      color: 'Orange',
      type: 'Sedan',
      engineNumber: 'ABC123456',
    },
  ];

  const listRequest: RequestDto = {
    paginate: false,
    pageSize: 1,
    pageNo: 10,
    sortOrder: SortOrderEnum.ASC,
    sortBy: SortByEnum.CREATED_AT,
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VehicleController],
      providers: [
        {
          provide: VehicleService,
          useValue: mockFunctions,
        },
      ],
    }).compile();

    controller = module.get<VehicleController>(VehicleController);
  });

  it('Should create vehicle', async () => {
    jest.spyOn(mockFunctions, 'create').mockReturnValue(vehicles[0]);
    const result = await controller.create(createRequest);

    expect(result).toBe(vehicles[0]);
    expect(mockFunctions.create).toHaveBeenCalled();
  });

  it('Should list all vehicles', async () => {
    jest.spyOn(mockFunctions, 'findAll').mockReturnValue(vehicles);
    const result = await controller.findAll(listRequest);

    expect(result).toBe(vehicles);
    expect(mockFunctions.findAll).toHaveBeenCalled();
  });

  it('Should list single vehicle', async () => {
    jest.spyOn(mockFunctions, 'findById').mockReturnValue(vehicles[0]);
    const result = await controller.findById('dsd88787');

    expect(result).toBe(vehicles[0]);
    expect(mockFunctions.findById).toHaveBeenCalled();
  });

  it('Should delete vehicle', async () => {
    jest.spyOn(mockFunctions, 'remove').mockReturnValue(undefined);
    const result = await controller.remove('dsd88787');

    expect(result).toBe(undefined);
    expect(mockFunctions.remove).toHaveBeenCalled();
  });

  it('Should update vehicle', async () => {
    jest.spyOn(mockFunctions, 'update').mockReturnValue(undefined);
    const result = await controller.update('dsd88787', updateRequest);

    expect(result).toBe(undefined);
    expect(mockFunctions.update).toHaveBeenCalled();
  });
});
