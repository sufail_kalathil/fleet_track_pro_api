openapi: 3.0.0
info:
  title: Fleet Tracker Pro API
  description: APIs for handling vehicle tracking
  version: 1.0.0
servers:
  - url: http://localhost:8080/api
tags:
  - name: Vehicle
    description: APIs for manage vehicles
  - name: Maintenance
    description: APIs for log vehicle maintenance history
  - name: Tracking
    description: APIs to track vehicle and sync real-time data
paths:
  /vehicle:
    post:
      tags:
        - Vehicle
      summary: Create a new vehicle
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Vehicle'
      responses:
        '201':
          description: Created
        '400':
          description: Validation failed
    get:
      tags:
        - Vehicle
      summary: Get all vehicles
      parameters:
        - name: pageNo
          in: query
          schema:
            type: integer
            description: Page number (optional)
        - name: pageSize
          in: query
          schema:
            type: integer
            description: Page size (optional)
        - name: sortOrder
          in: query
          schema:
            type: string
          description: Sort order (optional)
        - name: sortBy
          in : query
          schema:
            type: integer
            description: Sort by field (optional
        - name: paginate
          in : query
          schema:
            type: boolean
            description: pagination flag. Default is true

      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  result:
                    type: array
                    items:
                      $ref: '#/components/schemas/Vehicle'
                  pagination:
                    type: object
                    properties:
                      pageNo:
                        type: number
                        example: 1
                      pageSize:
                        type: number
                        example: 10
                      totalPages:
                        type: number
                        example: 5
                      totalItems:
                        type: number
                        example: 50
                      hasPreviousPage:
                        type: boolean
                        example: false
                      hasNextPage:
                        type: boolean
                        example: true

  /vehicle/{id}:
    get:
      tags:
        - Vehicle
      summary: Get a vehicle by ID
      parameters:
        - name: id
          in: path
          required: true
          description: ID of the vehicle to retrieve
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Vehicle'
        '404':
          description: Resource Not Found
    put:
      tags:
        - Vehicle
      summary: Update a vehicle by ID
      parameters:
        - name: id
          in: path
          required: true
          description: ID of the vehicle to update
          schema:
            type: string
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Vehicle'
      responses:
        '200':
          description: OK
        '404':
          description: Resource Not Found
    delete:
      tags:
        - Vehicle
      summary: Delete a vehicle by ID
      parameters:
        - name: id
          in: path
          required: true
          description: ID of the vehicle to delete
          schema:
            type: string
      responses:
        '200':
          description: OK
        '404':
          description: Resource Not Found !

  /vehicle/{id}/analytics:
    get:
      tags:
        - Vehicle
      summary: Get a vehicle analytics by ID
      parameters:
        - name: id
          in: path
          required: true
          description: ID of the vehicle to retrieve
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/VehicleAnalytics'
        '404':
          description: Resource Not Found
  /maintenance:
    post:
      tags:
        - Maintenance
      summary: Log maintenance for a vehicle
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Maintenance'
      responses:
        '201':
          description: Created
        '400':
          description: Validation failed
    get:
      tags:
        - Maintenance
      summary: Get all maintenance logs
      parameters:
        - name: pageNo
          in: query
          schema:
            type: integer
            description: Page number (optional)
        - name: pageSize
          in: query
          schema:
            type: integer
            description: Page size (optional)
        - name: sortOrder
          in: query
          schema:
            type: string
          description: Sort order (optional)
        - name: sortBy
          in: query
          schema:
            type: integer
            description: Sort by field (optional
        - name: paginate
          in: query
          schema:
            type: boolean
            description: pagination flag. Default is true
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  result:
                    type: array
                    items:
                      $ref: '#/components/schemas/Maintenance'
                  pagination:
                    type: object
                    properties:
                      pageNo:
                        type: number
                        example: 1
                      pageSize:
                        type: number
                        example: 10
                      totalPages:
                        type: number
                        example: 5
                      totalItems:
                        type: number
                        example: 50
                      hasPreviousPage:
                        type: boolean
                        example: false
                      hasNextPage:
                        type: boolean
                        example: true

  /maintenance/{id}:
    get:
      tags:
        - Maintenance
      summary: Get maintenance logs for a vehicle by ID
      parameters:
        - name: id
          in: path
          required: true
          description: ID of the vehicle to retrieve maintenance logs for
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Maintenance'
        '404':
          description: Resource Not Found
    put:
      tags:
        - Maintenance
      summary: Update a maintenance by ID
      parameters:
        - name: id
          in: path
          required: true
          description: ID of the maintenance to update
          schema:
            type: string
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Maintenance'
      responses:
        '200':
          description: OK
        '404':
          description: Resource Not Found


    delete:
      tags:
        - Maintenance
      summary: Delete a maintenance by ID
      parameters:
        - name: id
          in: path
          required: true
          description: ID of the maintenance to delete
          schema:
            type: string
      responses:
        '200':
          description: OK
        '404':
          description: Resource Not Found

  /maintenance/analytics/vehicle/{id}/cost:
    get:
      tags:
        - Maintenance
      summary: Get a vehicle analytics by ID
      parameters:
        - name: id
          in: path
          required: true
          description: ID of the vehicle to retrieve
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MaintenanceCost'





  /tracking/vehicle/{vehicleId}/start:
    post:
      tags:
        - Tracking
      summary: Start tracking a vehicle
      parameters:
        - name: vehicleId
          in: path
          required: true
          description: ID of the vehicle to start tracking
          schema:
            type: string
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/TrackingStart'
      responses:
        '201':
          description: Created
        '400':
          description: Validation failed
        '404':
          description: Resource Not Found

  /tracking/vehicle/{vehicleId}/log:
    post:
      tags:
        - Tracking
      summary: Log tracking data for a vehicle
      parameters:
        - name: vehicleId
          in: path
          required: true
          description: ID of the vehicle to log tracking data for
          schema:
            type: string
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/TrackingLog'
      responses:
        '201':
          description: Created
        '400':
          description: Validation failed
        '404':
          description: Resource Not Found
  /tracking/vehicle/{vehicleId}/stop:
    post:
      tags:
        - Tracking
      summary: Stop tracking a vehicle
      parameters:
        - name: vehicleId
          in: path
          required: true
          description: ID of the vehicle to stop tracking
          schema:
            type: string
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/TrackingStop'
      responses:
        '201':
          description: Created
        '400':
          description: Validation failed
        '404':
          description: Resource Not Found
components:
  schemas:
    VehicleAnalytics:
      type: object
      properties:
        totalDistance:
          type: number
        totalDuration:
          type: number
    MaintenanceCost:
      type: object
      properties:
        totalCost:
          type: number
    Vehicle:
      type: object
      properties:
        brand:
          type: string
        model:
          type: string
        year:
          type: integer
        uin:
          type: string
        color:
          type: string
        type:
          type: string
        engineNumber:
          type: string
      required:
        - brand
        - model
        - year
        - uin
    Maintenance:
      type: object
      properties:
        vehicleId:
          type: string
        serviceDate:
          type: string
          format: date-time
        serviceType:
          type: string
        serviceProvider:
          type: string
        serviceCost:
          type: number
        notes:
          type: string
      required:
        - vehicleId
        - serviceDate
        - serviceType
        - serviceProvider
        - serviceCost
    TrackingStart:
      type: object
      properties:
        locationName:
          type: string
        latitude:
          type: number
        longitude:
          type: number
      required:
        - locationName
        - latitude
        - longitude
    TrackingLog:
      type: object
      properties:
        locationName:
          type: string
        latitude:
          type: number
        longitude:
          type: number
        speed:
          type: number
      required:
        - locationName
        - latitude
        - longitude
    TrackingStop:
      type: object
      properties:
        locationName:
          type: string
        latitude:
          type: number
        longitude:
          type: number
      required:
        - locationName
        - latitude
        - longitude
